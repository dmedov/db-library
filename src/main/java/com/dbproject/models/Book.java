package com.dbproject.models;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by dpr on 20/04/15.
 */
@XmlRootElement(name = "book")
public class Book implements Serializable {
    @XmlElement
    private int price;

    @XmlElement
    private String name;

    @XmlElement
    private Date date;

    @XmlTransient
    private int id;

    @XmlTransient
    private int authorId;

    public Book() {
    }

    public Book(String name, int price, String date) {
        this.price = price;
        this.name = name;

        if (date != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            try {
                this.date = simpleDateFormat.parse(date);
            } catch (ParseException e) {
                throw new RuntimeException("can't parse date", e);
            }
        }
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public Date getDate() {
        return date;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @XmlTransient
    public int getId() {
        return id;
    }

    @XmlTransient
    public int getAuthorId() {
        return authorId;
    }
}
