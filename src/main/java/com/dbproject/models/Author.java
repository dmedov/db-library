package com.dbproject.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by dpr on 21/04/15.
 */
public class Author {
    private String name;
    private Date birth;
    private Date death;
    private int id;

    public Author(String name, String birth, String death) {
        this.name = name;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            this.birth = simpleDateFormat.parse(birth);
            this.death = simpleDateFormat.parse(death);
        } catch (ParseException e) {
            throw new RuntimeException("can't parse date", e);
        }
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Date getBirth() {
        return birth;
    }

    public Date getDeath() {
        return death;
    }

    public boolean isAlive() {
        return true;
    }
}
