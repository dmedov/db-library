package com.dbproject.services;

import com.dbproject.dao.BookstoreDao;
import com.dbproject.exporters.BooksFileExporter;
import com.dbproject.importers.BooksFileImporter;
import com.dbproject.models.Book;
import com.googlecode.ehcache.annotations.Cacheable;
import com.dbproject.exporters.FileFormat;
import com.dbproject.models.Author;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by dpr on 21/04/15.
 */
@Service
public class BookstoreServiceImpl implements BookstoreService {
    private static final Logger logger = LoggerFactory.getLogger(BookstoreServiceImpl.class);

    @Autowired
    BookstoreDao bookstoreDao;

    @Autowired
    List<BooksFileExporter> fileExporters;

    @Autowired
    List<BooksFileImporter> fileImporters;

    @Override
    public List<Book> getAllBooks() {
        return bookstoreDao.getAllBooks();
    }

    @Override
    public List<Author> getAllAuthors() {
        return bookstoreDao.getAllAuthors();
    }

    public void saveBooks(String fileName, FileFormat fileFormat) {
        for (BooksFileExporter fileGenerator : fileExporters) {
            if (fileGenerator.type() == fileFormat) {
                fileGenerator.generate(getAllBooks(), fileName);
                break;
            }
        }
    }

    @Override
    @Transactional
    public void insert(Book book) {
        bookstoreDao.insert(book);
    }

    @Override
    @Transactional
    public void insert(Author author) {
        bookstoreDao.insert(author);
    }

    @Override
    @Cacheable(cacheName = "books")
    public Book findBook(int id) {
        Book book = bookstoreDao.findBook(id);
        logger.info("try find book with id " + id);
        return book;
    }

    @Override
    @Transactional
    @CacheEvict(value = "books")
    public void removeBook(int bookId) {
        bookstoreDao.removeBook(bookId);
        logger.info("remove book with id " + bookId);
    }

    @Override
    @Cacheable(cacheName = "authors")
    public Author findAuthor(int id) {
        return bookstoreDao.findAuthor(id);
    }

    @Override
    @Transactional
    @CacheEvict(value = "authors")
    public void removeAuthor(int authorId) {
        bookstoreDao.removeAuthor(authorId);
        logger.info("remove author with id " + authorId);
    }

    @Override
    public Book findBookByName(String name) {
        return bookstoreDao.findBookByName(name).get(0);
    }

    @Override
    public void setDao(BookstoreDao dao) {
        this.bookstoreDao = dao;
    }

    @Override
    public void importBooks(String fileName) {
    }
}
