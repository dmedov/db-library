package com.dbproject.services;

import com.dbproject.dao.BookstoreDao;
import com.dbproject.models.Book;
import com.dbproject.exporters.FileFormat;
import com.dbproject.models.Author;

import java.util.List;

/**
 * Created by dpr on 21/04/15.
 */
public interface BookstoreService {
    List<Book> getAllBooks();

    List<Author> getAllAuthors();

    void insert(Book book);

    void insert(Author author);

    void removeBook(int bookId);

    void removeAuthor(int authorId);

    void saveBooks(String fileName, FileFormat fileFormat);

    Book findBook(int id);

    Author findAuthor(int id);

    Book findBookByName(String name);

    void setDao(BookstoreDao dao);

    void importBooks(String fileName);
}
