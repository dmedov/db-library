package com.dbproject.exporters;

/**
 * Created by dpr on 22/04/15.
 */
public enum FileFormat {
    TXT, XML
}
