package com.dbproject.exporters;

import com.dbproject.models.Book;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by dpr on 22/04/15.
 */
@Component
public class TxtFileExporter implements BooksFileExporter {
    @Override
    public void generate(List<Book> books, String fileName) {
        try (FileWriter fileWriter = new FileWriter(fileName)){

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            for (Book book : books) {
                fileWriter.write(book.getName() + " " + simpleDateFormat.format(book.getDate()) + " " + book.getPrice() + "\n");
            }
        } catch (IOException e) {
            throw new RuntimeException("can't write to file " + fileName);
        }
    }

    @Override
    public FileFormat type() {
        return FileFormat.TXT;
    }
}
