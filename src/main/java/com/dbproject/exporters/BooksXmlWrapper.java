package com.dbproject.exporters;

import com.dbproject.models.Book;
import org.springframework.stereotype.Component;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by dpr on 22/04/15.
 */
@Component
@XmlRootElement(name = "books")
public class BooksXmlWrapper {

    @XmlElement(name = "book")
    private List<Book> books;

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public List<Book> getBooks() {
        return books;
    }
}
