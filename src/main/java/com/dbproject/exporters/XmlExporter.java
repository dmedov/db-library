package com.dbproject.exporters;

import com.dbproject.models.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.FileWriter;
import java.util.List;

/**
 * Created by dpr on 22/04/15.
 */
@Component
public class XmlExporter implements BooksFileExporter {

    @Autowired
    BooksXmlWrapper booksXmlWrapper;

    public void generate(List<Book> books, String fileName) {
        try (FileWriter fileWriter = new FileWriter(fileName)) {
            JAXBContext jaxbContext = JAXBContext.newInstance(BooksXmlWrapper.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            booksXmlWrapper.setBooks(books);

            jaxbMarshaller.marshal(booksXmlWrapper, fileWriter);
        } catch (Exception e) {
            throw new RuntimeException("can't generate xml", e);
        }
    }

    @Override
    public FileFormat type() {
        return FileFormat.XML;
    }
}
