package com.dbproject.exporters;

import com.dbproject.models.Book;

import java.util.List;

/**
 * Created by dpr on 22/04/15.
 */
public interface BooksFileExporter {
    void generate(List<Book> books, String fileName);
    FileFormat type();
}
