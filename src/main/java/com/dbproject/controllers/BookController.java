package com.dbproject.controllers;

import com.dbproject.models.Book;
import com.dbproject.services.BookstoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by dpr on 20/04/15.
 */
@Controller
@RequestMapping("/editBook")
public class BookController extends MainController {

    @Autowired
    private BookstoreService bookService;

    @RequestMapping("/{id}")
    public String findBook(@PathVariable int id, ModelMap model) {
        model.addAttribute("book", bookService.findBook(id));
        return "editBook";
    }

    @RequestMapping("/find")
    public String findBookByName(ModelMap model, HttpServletRequest request) {
        Book foundBook = bookService.findBookByName(request.getParameter("name"));
        if (foundBook == null) {
            return "bookNotFound";
        }
        model.addAttribute("book", foundBook);
        return "book";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String createBook() {
        return "createBook";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createBook(HttpServletRequest request, ModelMap model) {
        Book book = new Book();
        book.setName(request.getParameter("name"));
        book.setPrice(Integer.parseInt(request.getParameter("price")));

        bookService.insert(book);

        return showMainPage(model);
    }
}
