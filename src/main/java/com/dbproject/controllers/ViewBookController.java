package com.dbproject.controllers;

import com.dbproject.services.BookstoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by dpr on 20/04/15.
 */
@Controller
@RequestMapping("/books")
public class ViewBookController extends MainController {
    @Autowired
    private BookstoreService bookService;

    @RequestMapping("/{id}")
    public String findBook(@PathVariable int id, ModelMap model){
        System.out.println("id = " + id);
        model.addAttribute("book", bookService.findBook(id));
        return "book";
    }

    @RequestMapping("/{id}/delete")
    public String deleteBook(@PathVariable int id, ModelMap model) {
        bookService.removeBook(id);
        return showMainPage(model);
    }
}






