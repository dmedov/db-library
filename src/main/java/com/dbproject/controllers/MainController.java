package com.dbproject.controllers;

import com.dbproject.models.Book;
import com.dbproject.services.BookstoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by dpr on 20/04/15.
 */
@Controller
@RequestMapping("/")
public class MainController {
    @Autowired
    private BookstoreService bookstoreService;

    @RequestMapping(method = RequestMethod.GET)
    public String showMainPage(ModelMap model) {
        List<Book> allBooks = bookstoreService.getAllBooks();
        model.addAttribute("books", allBooks);
        return "main";
    }
}
