package com.dbproject.dao;

import com.dbproject.models.Book;
import com.dbproject.models.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dpr on 20/04/15.
 */
@Repository
public class BookstoreDaoImpl implements BookstoreDao {
    private static final String BOOKS_TABLE_NAME = "BOOKS";
    private static final String AUTHORS_TABLE_NAME = "AUTHORS";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("books_jdbc_insert")
    private SimpleJdbcInsert booksJdbcInsert;

    @Autowired
    @Qualifier("authors_jdbc_insert")
    private SimpleJdbcInsert authorsJdbcInsert;

    @Autowired
    private BookMapper bookMapper;

    @Autowired
    private AuthorMapper authorMapper;

    @PostConstruct
    public void init() {
        booksJdbcInsert.withTableName(BOOKS_TABLE_NAME);
        booksJdbcInsert.setGeneratedKeyName("id");
        authorsJdbcInsert.withTableName(AUTHORS_TABLE_NAME);
        authorsJdbcInsert.setGeneratedKeyName("id");
    }

    @Override
    public List<Book> getAllBooks() {
        return jdbcTemplate.query("select * from " + BOOKS_TABLE_NAME, bookMapper);
    }

    @Override
    public List<Author> getAllAuthors() {
        return jdbcTemplate.query("select * from " + AUTHORS_TABLE_NAME, authorMapper);
    }

    @Override
    public void insert(Book book) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("name", book.getName());
        parameters.put("price", book.getPrice());

        String date = new SimpleDateFormat("yyyy-MM-dd").format(book.getDate());
        parameters.put("publish", date);
        parameters.put("author_id", 1);

        booksJdbcInsert.execute(parameters);
    }

    @Override
    public void insert(Author author) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("name", author.getName());

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        String birth = simpleDateFormat.format(author.getBirth());
        parameters.put("birth", birth);

        String death = simpleDateFormat.format(author.getBirth());
        parameters.put("death", death);

        authorsJdbcInsert.execute(parameters);
    }

    @Override
    public Author findAuthor(int id) {
        return jdbcTemplate.queryForObject("select * from " + AUTHORS_TABLE_NAME + " where id=?", new Object[]{id}, authorMapper);
    }

    @Override
    public void removeAuthor(int authorId) {
        jdbcTemplate.update("delete from " + AUTHORS_TABLE_NAME + " where id=?", authorId);
    }

    @Override
    public List<Book> getAllBooksWithPriceGreaterThan(int price) {
        return jdbcTemplate.query("select * from " + BOOKS_TABLE_NAME + " where price > " + price, bookMapper);
    }

    @Override
    public Book findBook(int id) {
        return jdbcTemplate.queryForObject("select * from " + BOOKS_TABLE_NAME + " where id=?", new Object[]{id}, bookMapper);
    }

    @Override
    public void removeBook(int bookId) {
        jdbcTemplate.update("delete from " + BOOKS_TABLE_NAME + " where id=?", bookId);
    }

    @Override
    public List<Book> findBookByName(String name) {
        return jdbcTemplate.query("select * from " + BOOKS_TABLE_NAME + " where name like\'%" + name + "%\'", bookMapper);
    }
}
