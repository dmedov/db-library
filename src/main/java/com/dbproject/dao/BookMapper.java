package com.dbproject.dao;

import com.dbproject.models.Book;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by dpr on 20/04/15.
 */
@Component
public class BookMapper implements RowMapper<Book> {
    public Book mapRow(ResultSet resultSet, int i) throws SQLException {
        Book book = new Book(resultSet.getString("name"), resultSet.getInt("price"), resultSet.getString("publish"));
        book.setAuthorId(resultSet.getInt("author_id"));
        book.setId(resultSet.getInt("id"));
        return book;
    }
}
