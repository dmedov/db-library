package com.dbproject.dao;

import com.dbproject.models.Author;
import com.dbproject.models.Book;

import java.util.List;

/**
 * Created by dpr on 21/04/15.
 */
public interface BookstoreDao {
    List<Book> getAllBooks();

    List<Author> getAllAuthors();

    void insert(Book book);

    void insert(Author author);

    void removeBook(int bookId);

    void removeAuthor(int authorId);

    Book findBook(int id);

    List<Book> findBookByName(String name);

    Author findAuthor(int id);

    List<Book> getAllBooksWithPriceGreaterThan(int price);
}
