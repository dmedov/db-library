package com.dbproject.dao;

import com.dbproject.models.Author;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by dpr on 21/04/15.
 */
@Component
public class AuthorMapper implements RowMapper<Author> {
    public Author mapRow(ResultSet resultSet, int i) throws SQLException {
        Author author = new Author(resultSet.getString("name"), resultSet.getString("birth"), resultSet.getString("death"));
        author.setId(resultSet.getInt("id"));
        return author;
    }
}
