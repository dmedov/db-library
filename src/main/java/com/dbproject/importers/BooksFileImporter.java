package com.dbproject.importers;

/**
 * Created by dpr on 23/04/15.
 */
public interface BooksFileImporter {
    void importFromFile(String fileName);
}
