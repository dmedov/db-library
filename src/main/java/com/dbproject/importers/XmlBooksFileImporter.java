package com.dbproject.importers;

import com.dbproject.dao.BookstoreDao;
import com.dbproject.exporters.BooksXmlWrapper;
import com.dbproject.models.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * Created by dpr on 23/04/15.
 */
@Component
public class XmlBooksFileImporter implements BooksFileImporter {
    @Autowired
    BookstoreDao bookstoreDao;

    @Override
    public void importFromFile(String fileName) {
        try  {
            File file = new File(fileName);
            JAXBContext jaxbContext = JAXBContext.newInstance(BooksXmlWrapper.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            BooksXmlWrapper booksXmlWrapper = (BooksXmlWrapper) jaxbUnmarshaller.unmarshal(file);

            for (Book book : booksXmlWrapper.getBooks()) {
                bookstoreDao.insert(book);
            }

        } catch (Exception e) {
            throw new RuntimeException("can't generate xml", e);
        }
    }
}
