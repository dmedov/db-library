package com.dbproject.services;

import com.dbproject.dao.BookMapper;
import com.dbproject.dao.BookstoreDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by dpr on 23/04/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/mvc-dispatcher-servlet.xml"})
@ActiveProfiles("test")
public class BookstoreServiceImplTest {
    @Autowired
    BookstoreService bookstoreService;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    BookMapper bookMapper;

    private BookstoreDao mockDao;

    @Before
    public void setUp() throws Exception {
        mockDao = mock(BookstoreDao.class);
        when(mockDao.getAllBooks()).thenReturn(jdbcTemplate.query("select * from books", bookMapper));

        bookstoreService.setDao(mockDao);
    }

    @Test
    public void testFindBookByName() throws Exception {
        assertEquals("Kapitanskaya", bookstoreService.findBookByName("pit").getName());
    }

    @Test
    public void testCache() throws Exception {
        bookstoreService.findBook(10);
        bookstoreService.findBook(10);

        verify(mockDao, times(1)).findBook(anyInt());
    }
}