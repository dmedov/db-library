package com.dbproject.dao;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

/**
 * Created by dpr on 22/04/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/mvc-dispatcher-servlet.xml"})
@ActiveProfiles("test")
public class BookstoreDaoImplTest {
    @Autowired
    BookstoreDao bookstoreDao;

    @Test
    public void testGetAllBooks() throws Exception {
        assertEquals(3, bookstoreDao.getAllBooks().size());
    }

    @Test
    public void testFindBook() throws Exception {
        Assert.assertEquals("Onegin", bookstoreDao.findBook(1).getName());
    }

    @Test
    public void testGetAllBooksWithPriceGreaterThan() throws Exception {
        assertEquals(2, bookstoreDao.getAllBooksWithPriceGreaterThan(15).size());
    }

    @Test
    @Transactional
    public void testRemoveBook() throws Exception {
        bookstoreDao.removeBook(1);
        assertEquals(2, bookstoreDao.getAllBooks().size());
    }
}